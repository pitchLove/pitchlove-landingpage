;(function(){

			// Menu settings
			$('#menuToggle, .menu-close').on('click', function(){
				$('#menuToggle').toggleClass('active');
				$('body').toggleClass('body-push-toleft');
				$('#theMenu').toggleClass('menu-open');
			});

			var maxHeight = Math.max.apply(null, $("div.available").map(function () {
			    return $(this).height();
			}).get());

			$('.available').height(maxHeight);

			$('#main-subscribe').submit(function() {
             $.ajax({
                 type: "POST",
                  url: "gethelp.html",
                  data: $(this).serialize(),
                  success: function() {
                    $("#main-subscribe").hide();
                    $("#redirect").html('<h3>Thanks for signing up! Let us know some more about your startup <a href="gethelp.html"><b>HERE</b></a>.</h3> ');
                    console.log("It worked!");
                   }
                })
            });

})(jQuery)